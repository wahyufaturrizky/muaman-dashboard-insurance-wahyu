<!DOCTYPE html>
<html>
<head>
  <title>Sertifikat</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
  <style type="text/css">
	#sertifikat {
		size: A4 landscape;
		position: absolute;
		font-size: 11pt;
		font-family: Arial;
	}
	#content {
		margin-top: 100px;
		margin-left: 50px;
		margin-right: 50px;
	}
	td{
		border: thin solid;
	}
  </style>
</head>
<body>
	<?php //var_dump($header);?>
	<div id="sertifikat" style="border: solid 1px">
		<div id="content">
		<p>Lampiran <?php echo $no_sertifikat;?>
		<br><?php echo $pemegang;?>
		<br>Periode <?php echo $periode;?></p>
		<p>
		<table border="0">
			<tr> 
				<td style="padding:5px"><b>Nama Lengkap</b></td>
				<td style="padding:5px"><b>Tanggal Lahir</b></td>
				<td style="padding:5px"><b>Usia</b></td>
				<td style="padding:5px"><b>Mp (dalam bulan)</b></td>
				<td style="padding:5px"><b>Mulai Perjanjian</b></td>	
				<td style="padding:5px"><b>Akhir Perjanjian</b></td>
				<td style="padding:5px"><b>Manfaat Asuransi Awal</b></td>
				<td style="padding:5px"><b>Kontribusi</b></td>
				<td style="padding:5px"><b>Tanggal Persetujuan</b></td>
				<!-- <td style="padding:5px"><b>Ketentuan Underwriting</b></td> -->
			</tr>
			<tbody>
				<?php foreach ($purchase_data->result() as $b) :?>
					<tr>
						<td style="padding:5px"><?php echo $b->namapeserta ?> </td>
						<td style="padding:5px"><?php echo $b->tanggallahir ?> </td>
						<td style="padding:5px"><?php echo $b->usia ?> </td>	
						<td style="padding:5px"><?php echo $b->mp_bulan ?> </td>
						<td style="padding:5px"><?php echo $b->mulaiperjanjian ?> </td>
						<td style="padding:5px"><?php echo $b->akhirperjanjian ?> </td>
						<td style="padding:5px"><?php echo $b->manfaatasuransiawal ?> </td>
						<td style="padding:5px"><?php echo $b->kontribusi ?> </td>
						<td style="padding:5px"><?php echo $b->tanggal_disetujui ?> </td>
						<!-- <td style="padding:5px"><?php echo $b->ketentuan_u ?> </td> -->
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		</p>
		<p>Total Pertanggungan : Rp. <?php echo number_format($pertanggungan); ?>
		<br>Total Premi : Rp. <?php echo number_format($premi); ?></p>
		</div>
	</div>





	<script type="text/javascript">
  	$( document ).ready(function() {
	    console.log( "ready!" );
	    saveAsPDF();
	});

  	function saveAsPDF() {
	    html2canvas(document.getElementById("sertifikat"), {
	    	onrendered: function(canvas) {
	    		var imgData = canvas.toDataURL('image/png');
	    		window.open(imgData);
		        var pdf = new jsPDF('p', 'mm', 'a4');
		        var pageWidth = pdf.internal.pageSize.width;
		        var pageHeight = pdf.internal.pageSize.height;
		        var imageWidth = canvas.width;
		        var imageHeight = canvas.height;

		        var ratio = imageWidth/imageHeight >= pageWidth/pageHeight ? pageWidth/imageWidth : pageHeight/imageHeight;
		        /*pdf.addHTML(document.body,function() {
				    console.log("started");
				    pdf.save('demo.pdf')
				    console.log("finished");
				});*/
		        pdf.addImage(imgData, 'JPEG', 0, 0, imageWidth * ratio, imageHeight * ratio);
		        pdf.save('lampiran.pdf');
	    	}
	    });

	}
  </script>
</body>
</html>