<!-- Start Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- Start Main content -->
	<section class="content">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Dashboard Muaman
				<small>Version 1.0</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</section>

		<section class="content">
			<!-- Start Card Component Member Open, Approve, Pending and Cancel  -->
			<div class="row">

				<!-- Start Card Component Member Open -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-green">
						<span class="info-box-icon"><i class="fa fa-user"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">MEMBER OPEN</span>
							<span class="info-box-number">41,410</span>
							<div class="progress">
								<div class="progress-bar" style="width: 70%"></div>
							</div>
							<span class="progress-description">
								70% Increase in 30 Days
							</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Member Open -->
			
				<!-- Start Card Component Member Approve -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-aqua">
						<span class="info-box-icon"><i class="fa fa-check-circle-o"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">MEMBER APPROVE</span>
							<span class="info-box-number">41,410</span>
							<div class="progress">
								<div class="progress-bar" style="width: 70%"></div>
							</div>
							<span class="progress-description">
								70% Increase in 30 Days
							</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Member Approve -->

				<!-- Start Card Component Member Pending -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-yellow">
						<span class="info-box-icon"><i class="fa fa-user-circle-o"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">MEMBER PENDING</span>
							<span class="info-box-number">41,410</span>
							<div class="progress">
								<div class="progress-bar" style="width: 70%"></div>
							</div>
							<span class="progress-description">
								70% Increase in 30 Days
							</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Member Pending -->

				<!-- Start Card Component Member Cancel -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-red">
						<span class="info-box-icon"><i class="fa fa-user-times"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">MEMBER CANCEL</span>
							<span class="info-box-number">41,410</span>
							<div class="progress">
								<div class="progress-bar" style="width: 70%"></div>
							</div>
							<span class="progress-description">
								70% Increase in 30 Days
							</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Member Cancel -->

			</div>
			<!-- End Card Component Member Open, Approve, Pending and Cancel  -->

			<!-- Start Card Component Pengajuan Aktif dan Pengajuan Arsip -->
			<div class="row">

				<!-- Start Card Component Pengajuan Aktif -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Pengajuan Aktif</span>
							<span class="info-box-number">760</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Pengajuan Aktif -->

				<!-- Start Card Component Pengajuan Arsip -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-green"><i class="fa fa-file-archive-o"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Pengajuan Arsip</span>
							<span class="info-box-number">760</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Pengajuan Arsip -->

				<!-- fix for small devices only -->
				<div class="clearfix visible-sm-block"></div>

				<!-- Start Card Component Pengajuan Arsip -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-aqua"><i class="fa fa-credit-card"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Premi</span>
							<span class="info-box-number">760</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Pengajuan Arsip -->

				<!-- Start Card Component Pengajuan Arsip -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Total Pinjaman</span>
							<span class="info-box-number">760</span>
						</div>
					</div>
				</div>
				<!-- End Card Component Pengajuan Arsip -->

			</div>
			<!-- End Card Component Pengajuan Aktif dan Pengajuan Arsip -->

			<!-- Start Card Component Grafik Area Chart dan Pie Chart Perbandingan Pengajuan Aktif dan Pengajuan Arsip -->
			<div class="row">
				<!-- Start Card Component Grafik Area Chart Aktif dan Pengajuan Arsip -->
				<div class="col-md-8">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li class="active"><a href="#chart-Pengajuan-Harian" data-toggle="tab">Harian</a></li>
							<li><a href="#chart-Pengajuan-Bulanan" data-toggle="tab">Bulanan</a></li>
							<li><a href="#chart-Pengajuan-Tahunan" data-toggle="tab">Tahunan</a></li>
							<li class="pull-left header"><i class="fa fa-area-chart"></i> Grafik Perbandingan Pengajuan Aktif dan Arsip</li>
						</ul>
						<div class="tab-content chart">
							<!-- START Date Range Picker -->
							<div class="input-group pull-right">
								<button class="btn btn-default" id="daterange-btn">
									<i class="fa fa-calendar"></i> Date range picker
									<i class="fa fa-caret-down"></i>
								</button>
							</div>
							<!-- END Date Range Picker -->

							<!-- Chart JS - Grafik Pengajuan -->
							<canvas class="chart tab-pane active" id="chart-Pengajuan-Harian" style="position: relative; height: 340px;"></canvas>
							<canvas class="chart tab-pane" id="chart-Pengajuan-Bulanan" style="position: relative; height: 300px;"></canvas>
							<canvas class="chart tab-pane" id="chart-Pengajuan-Tahunan" style="position: relative; height: 300px;"></canvas>
							<ul class="chart-legend text-center list-inline" style="margin-top: 16px;">
								<li><i class="fa fa-circle-o text-green"></i> Pengajuan Aktif</li>
								<li><i class="fa fa-circle-o" style="color: rgba(230, 204, 255, 1)"></i> Pengajuan Arsip</li>
							</ul>
						</div>
					</div><!-- /.nav-tabs-custom -->
				</div>
				<!-- End Card Component Grafik Area Chart Aktif dan Pengajuan Arsip -->

				<!-- Start Card Component Pie Chart Aktif dan Pengajuan Arsip -->
				<div class="col-md-4">
					<div class="box box-success">
						<div class="box-header with-border">
							<i class="fa fa-pie-chart"></i>
							<h3 class="box-title">Pie Chart Pengajuan Aktif dan Arsip</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div><!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-8">
									<div class="chart-responsive">
										<canvas id="pieChart" height="255"></canvas>
									</div><!-- ./chart-responsive -->
								</div><!-- /.col -->
								<div class="col-md-4">
									<ul class="chart-legend clearfix">
										<li><i class="fa fa-circle-o text-green"></i> Pengajuan Aktif</li>
										<li><i class="fa fa-circle-o" style="color: rgba(230, 204, 255, 1)"></i> Pengajuan Arsip</li>
									</ul>
								</div><!-- /.col -->
							</div><!-- /.row -->
						</div><!-- /.box-body -->
						<div class="box-footer no-padding">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Pengajuan Aktif <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 80%</span></a></li>
								<li><a href="#">Pengajuan Arsip <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 4%</span></a></li>
							</ul>
						</div><!-- /.box-footer -->
					</div><!--/.box -->
				</div>
				<!-- End Card Component Pie Chart Aktif dan Pengajuan Arsip -->

			</div>
			<!-- End Card Component Grafik Area Chart dan Pie Chart Perbandingan Pengajuan Aktif dan Pengajuan Arsip -->

			<!-- Start Card Component Grafik Area Chart dan Pie Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->
			<div class="row">
				<!-- Start Card Component Pie Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->
				<div class="col-md-4">
					<div class="box box-success">
						<div class="box-header with-border">
							<i class="fa fa-pie-chart"></i>
							<h3 class="box-title">Pie Chart Status Member Muaman</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
								<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div><!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-8">
									<div class="chart-responsive">
										<canvas id="pieChartDua" height="255"></canvas>
									</div><!-- ./chart-responsive -->
								</div><!-- /.col -->
								<div class="col-md-4">
									<ul class="chart-legend clearfix">
										<li><i class="fa fa-circle-o text-green"></i> Memper Open</li>
										<li><i class="fa fa-circle-o" style="color: rgba(46, 175, 244, 1)"></i> Member Approve</li>
										<li><i class="fa fa-circle-o" style="color: rgba(249, 189, 99, 1)"></i> Member Pending</li>
										<li><i class="fa fa-circle-o" style="color: rgba(230, 204, 255, 1)"></i> Member Cancel</li>
									</ul>
								</div><!-- /.col -->
							</div><!-- /.row -->
						</div><!-- /.box-body -->
						<div class="box-footer no-padding">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Memper Open <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 80%</span></a></li>
								<li><a href="#">Member Approve <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 14%</span></a></li>
								<li><a href="#">Member Pending <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 80%</span></a></li>
								<li><a href="#">Member Cancel <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
							</ul>
						</div><!-- /.box-footer -->
					</div><!--/.box -->
				</div>
				<!-- End Card Component Pie Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->
				
				<!-- Start Card Component Grafik Area Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->
				<div class="col-md-8">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li><a href="#chartAreaStatusMemberHarian" data-toggle="tab">Harian</a></li>
							<li  class="active"><a href="#chartAreaStatusMemberBulanan" data-toggle="tab">Bulanan</a></li>
							<li><a href="#chartAreaStatusMemberTahunan" data-toggle="tab">Tahunan</a></li>
							<li class="pull-left header"><i class="fa fa-bar-chart"></i> Grafik Status Member Muaman</li>
						</ul>
						<div class="tab-content chart">
							<!-- START Date Range Picker -->
							<div class="input-group pull-right">
								<button class="btn btn-default" id="daterange-btn2">
									<i class="fa fa-calendar"></i> Date range picker
									<i class="fa fa-caret-down"></i>
								</button>
							</div>
							<!-- END Date Range Picker -->

							<!-- Chart JS - Grafik Pengajuan -->
							<canvas class="chart tab-pane" id="chartAreaStatusMemberHarian" style="position: relative; height: 340px;"></canvas>
							<canvas class="chart tab-pane active" id="chartAreaStatusMemberBulanan" style="position: relative; height: 342px;"></canvas>
							<canvas class="chart tab-pane" id="chartAreaStatusMemberTahunan" style="position: relative; height: 300px;"></canvas>
							<ul class="chart-legend clearfix text-center list-inline">
								<li><i class="fa fa-circle-o text-green" style="margin-top: 16px;"></i> Memper Open</li>
								<li><i class="fa fa-circle-o" style="color: rgba(46, 175, 244, 1)"></i> Member Approve</li>
								<li><i class="fa fa-circle-o" style="color: rgba(249, 189, 99, 1)"></i> Member Pending</li>
								<li><i class="fa fa-circle-o" style="color: rgba(230, 204, 255, 1)"></i> Member Cancel</li>
							</ul>
						</div>
					</div><!-- /.nav-tabs-custom -->
				</div>
				<!-- End Card Component Grafik Area Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->

			</div>
			<!-- End Card Component Grafik Area Chart dan Pie Chart Perbandingan Member = OPEN, APPROVE, PENDING, CANCEL -->

			<!-- Start Card Component Grafik LINE Chart TOTAL PREMI DAN TOTAL PINJAMAN -->
			<div class="row">
				
				<!-- Start Card Component Grafik LINE Chart TOTAL PREMI -->
				<div class="col-md-6">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li><a href="#chartLinePremiHarian" data-toggle="tab">Harian</a></li>
							<li  class="active"><a href="#chartLinePremiBulanan" data-toggle="tab">Bulanan</a></li>
							<li><a href="#chartLinePremiTahunan" data-toggle="tab">Tahunan</a></li>
							<li class="pull-left header"><i class="fa fa-line-chart"></i> Grafik Status Premi</li>
						</ul>
						<div class="tab-content chart">
							<!-- START Date Range Picker -->
							<div class="input-group pull-right">
								<button class="btn btn-default" id="daterange-btn3">
									<i class="fa fa-calendar"></i> Date range picker
									<i class="fa fa-caret-down"></i>
								</button>
							</div>
							<!-- END Date Range Picker -->

							<!-- Chart JS - Grafik Premi -->
							<canvas class="chart tab-pane" id="chartLinePremiHarian" style="position: relative; height: 340px;"></canvas>
							<canvas class="chart tab-pane active" id="chartLinePremiBulanan" style="position: relative; height: 342px;"></canvas>
							<canvas class="chart tab-pane" id="chartLinePremiTahunan" style="position: relative; height: 300px;"></canvas>
							<ul class="chart-legend clearfix text-center list-inline">
								<li><i class="fa fa-circle-o" style="margin-top: 16px; color: rgba(46, 175, 244, 1);"></i> Premi</li>
							</ul>
						</div>
					</div><!-- /.nav-tabs-custom -->
				</div>
				<!-- End Card Component Grafik LINE Chart TOTAL PREMI -->

				<!-- Start Card Component Grafik LINE Chart TOTAL PINJAMAN -->
				<div class="col-md-6">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li><a href="#chartLinePinjamanHarian" data-toggle="tab">Harian</a></li>
							<li  class="active"><a href="#chartLinePinjamanBulanan" data-toggle="tab">Bulanan</a></li>
							<li><a href="#chartLinePinjamanTahunan" data-toggle="tab">Tahunan</a></li>
							<li class="pull-left header"><i class="fa fa-line-chart"></i> Grafik Status Pinjaman</li>
						</ul>
						<div class="tab-content chart">
							<!-- START Date Range Picker -->
							<div class="input-group pull-right">
								<button class="btn btn-default" id="daterange-btn4">
									<i class="fa fa-calendar"></i> Date range picker
									<i class="fa fa-caret-down"></i>
								</button>
							</div>
							<!-- END Date Range Picker -->

							<!-- Chart JS - Grafik Peminjaman -->
							<canvas class="chart tab-pane" id="chartLinePinjamanHarian" style="position: relative; height: 340px;"></canvas>
							<canvas class="chart tab-pane active" id="chartLinePinjamanBulanan" style="position: relative; height: 342px;"></canvas>
							<canvas class="chart tab-pane" id="chartLinePinjamanTahunan" style="position: relative; height: 300px;"></canvas>
							<ul class="chart-legend clearfix text-center list-inline">
								<li><i class="fa fa-circle-o" style="color: rgba(249, 189, 99, 1)"></i> Pinjaman</li>
							</ul>
						</div>
					</div><!-- /.nav-tabs-custom -->
				</div>
				<!-- End Card Component Grafik LINE Chart TOTAL PINJAMAN -->

			</div>
			<!-- End Card Component Grafik LINE Chart TOTAL PREMI DAN TOTAL PINJAMAN -->

			<!-- START TABLE: DAFTAR DAN STATUS MEMBER MUAMAN -->
			<div class="box box-success">
				<div class="box-header">
					<i class="fa fa-table"></i>
					<h3 class="box-title">Daftar dan Status Member Muaman</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-6">
								<div class="dataTables_length" id="example1_length"><label>Show <select name="example1_length"
											aria-controls="example1" class="form-control input-sm">
											<option value="10">10</option>
											<option value="25">25</option>
											<option value="50">50</option>
											<option value="100">100</option>
										</select> entries</label></div>
							</div>
							<div class="col-sm-6">
								<div id="example1_filter" class="dataTables_filter"><label>Search:<input type="search"
											class="form-control input-sm" placeholder="" aria-controls="example1"></label></div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<table id="example1" class="table table-bordered table-striped dataTable" role="grid"
									aria-describedby="example1_info">
									<thead>
										<tr role="row">
											<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
												aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending"
												style="width: 216px;">ID Member</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
												aria-label="Browser: activate to sort column ascending" style="width: 266px;">Tanggal Pembuatan</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
												aria-label="Platform(s): activate to sort column ascending" style="width: 235px;">Nama Lengkap</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
												aria-label="Engine version: activate to sort column ascending" style="width: 186px;">Status
											</th>
									</thead>
									<tbody>
										<tr role="row" class="odd">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-info">Approve</span></td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-info">Approve</span></td>
										</tr>
										<tr role="row" class="odd">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-warning">Pending</span></td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-warning">Pending</span></td>
										</tr>
										<tr role="row" class="odd">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-danger">Cancel</span></td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-danger">Cancel</span></td>
										</tr>
										<tr role="row" class="odd">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-success">Open</span></td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-success">Open</span></td>
										</tr>
										<tr role="row" class="odd">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-success">Open</span></td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1"><a href="pages/examples/invoice.html">OR9842</a></td>
											<td>23/12/2019</td>
											<td>Wahyu Fatur Rizki</td>
											<td><span class="label label-success">Open</span></td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<th rowspan="1" colspan="1">ID Member</th>
											<th rowspan="1" colspan="1">Tanggak Pembuatan</th>
											<th rowspan="1" colspan="1">Nama Lengkap</th>
											<th rowspan="1" colspan="1">Status</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57
									entries</div>
							</div>
							<div class="col-sm-7">
								<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
									<ul class="pagination">
										<li class="paginate_button previous disabled" id="example1_previous"><a href="#"
												aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
										<li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1"
												tabindex="0">1</a></li>
										<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a>
										</li>
										<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a>
										</li>
										<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a>
										</li>
										<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a>
										</li>
										<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a>
										</li>
										<li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1"
												data-dt-idx="7" tabindex="0">Next</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
			</div>
			<!-- END TABLE: DAFTAR DAN STATUS MEMBER MUAMAN -->

			<!-- Start GRAFIK TOTAL NASABAH PER MASING-MASING TIPE NASABAH -->
			<div class="box">
				<div class="box-header with-border">
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
								class="fa fa-minus"></i></button>
					</div>
					<div class="box-body">
						<!-- CONTENT -->
						<div id="headerchart" align="center" style="width:100%">
							<h3 class="box-title">GRAFIK TOTAL NASABAH PER MASING-MASING TIPE NASABAH TANGGAL
								<?php echo date('d-m-Y');?></h3>
						</div>
						<div style="width: 100%;overflow: auto;margin-top: 50px">
							<div id="charthome" style="width:100%;"></div>
						</div>
						<!-- endCONTENT -->
					</div><!-- /.box-body -->
				</div>
			</div>
			<!-- End GRAFIK TOTAL NASABAH PER MASING-MASING TIPE NASABAH -->

		</section>

	</section>
	<!-- End Main content -->
</div>
<!-- End Content Wrapper. Contains page content -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<style type="text/css">
	$ {
		demo.css
	}
</style>
<script type="text/javascript">
	$(function () {
		// Create the chart
		$('#charthome').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ""
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Total Nasabah'
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y:.0f}  nasabah'
					}
				}
			},

			tooltip: {
				headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
				pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> nasabah dari keseluruhan nasabah<br/>'
			},

			series: [{
				name: 'Tipe Nasabah',
				colorByPoint: true,
				data: [

					<?php
					echo $result;
					?>
				]
			}]
		});
	});
</script>
