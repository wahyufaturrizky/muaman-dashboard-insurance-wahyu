<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class generatepdflampiran extends CI_Controller {

	public function download()
	{
		$this->load->model('polis/bulkpurchase/mbulkpurchase');
		$getpolisdata = $this->mbulkpurchase->getpolispdf(
			// base64_decode($this->input->get('nopolis')),
			// base64_decode($this->input->get('nodetail'))
			$this->input->get('nopolis'),
			$this->input->get('nodetail')
		);
		foreach ($getpolisdata->result() as $k) {
			$pemegang = $k->pemegang_polis;
			$periode = explode("-",$k->periode_start);
			if($periode[1]=='01'){
				$bulan = 'Januari';
			}elseif($periode[1]=='02'){
				$bulan = 'Februari';
			}elseif($periode[1]=='03'){
				$bulan = 'Maret';
			}elseif($periode[1]=='04'){
				$bulan = 'April';
			}elseif($periode[1]=='05'){
				$bulan = 'Mei';
			}elseif($periode[1]=='06'){
				$bulan = 'Juni';
			}elseif($periode[1]=='07'){
				$bulan = 'Juli';
			}elseif($periode[1]=='08'){
				$bulan = 'Agustus';
			}elseif($periode[1]=='09'){
				$bulan = 'September';
			}elseif($periode[1]=='10'){
				$bulan = 'Oktober';
			}elseif($periode[1]=='11'){
				$bulan = 'November';
			}elseif($periode[1]=='12'){
				$bulan = 'Desember';
			}
		}

		$get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->get('id_purchase'));
		foreach($get_produkid_bypurchase->result() as $g){
			$produk_id = $g->id_produk;
		}
		$polis_tb = "";
		if($produk_id == 23){
			$polis_tb = 'polis_muaman_tetap';
		}elseif($produk_id == 24){
			$polis_tb = 'polis_muaman_plusmenurun';
		}elseif($produk_id == 25){
			$polis_tb = 'polis_muaman_menurun';
		}elseif($produk_id == 27){
			$polis_tb = 'polis_muaman_jma_pembiayaan';
		}

		$getpurchasedata = $this->mbulkpurchase->getpurchasepdf_v(
			// base64_decode($this->input->get('id_purchase'))
			$this->input->get('id_purchase'), 
			$polis_tb,
			$this->input->get('checkbox')
		);

		$pertanggungan=0;
		$premi = 0;
		foreach ($getpurchasedata->result() as $b) {
			// $header = explode("|", $b->headerform);
			// $datamember = explode("|", $b->data);
            // for ($i=0; $i < count($header) ; $i++) {

	        //     if($header[$i] == 'Manfaat Asuransi Awal'){ 
	        //         $indexpertanggungan = $i;
	        //     }elseif($header[$i] == 'Kontribusi '){
	        //         $indexpremi = $i;
	        //     }
	        // }
	        $pertanggungan = $pertanggungan+$b->manfaatasuransiawal;
	        $premi = $premi + $b->kontribusi;
		}

		$tglsekarang = date('d F Y');

		//$this->load->library('pdfgenerator');
		$data = array(
			// "no_sertifikat" => base64_decode($this->input->get('nopolis')),
			"no_sertifikat" => $this->input->get('nopolis'),
			"pemegang" => $pemegang,
			"periode" => $bulan.' '.$periode[0],
			"premi" => $premi,
			"pertanggungan" => $pertanggungan,
			"purchase_data" => $getpurchasedata,
			"tglsekarang" => $tglsekarang
		);
 
	    $html = $this->load->view('generatepdflampiran',$data);
	    
	    //$this->pdfgenerator->generate($html,'contoh');
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */