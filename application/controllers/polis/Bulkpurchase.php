<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkpurchase extends MY_Controller {
	public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->model('polis/bulkpurchase/mbulkpurchase');
                date_default_timezone_set('Asia/Jakarta');
        }

	public function index()
	{
		$data['getemail'] = $this->mbulkpurchase->getemail();
		$data['getasuransi'] = $this->mbulkpurchase->getasuransi();
		$this->template();
		$this->load->view('polis/bulkpurchase/content',$data);
		$this->load->view('layout/footer');
		$this->load->view('polis/bulkpurchase/content_js');
	}


	public function getprodukasuransi()
    {
        $getprodukasuransi = $this->mbulkpurchase->getprodukasuransi(
            $this->input->post("asuransi")
        );

        echo '<option value= "" disabled selected>Produk Asuransi('.$getprodukasuransi->num_rows().')</option>';
        foreach ($getprodukasuransi->result() as $k) {
            echo '<option value="'.$k->id_produk.'">
                 '.$k->nama_produk.'
                 </option>';
        }
    }

    public function getpolisid()
    {
        $getpolisid = $this->mbulkpurchase->getpolisid(
            $this->input->post("asuransi"),
            $this->input->post("produk_id"),
            $this->session->userdata('userid')
        );

        echo '<option value= "">ID Polis('.$getpolisid->num_rows().')</option>';
        foreach ($getpolisid->result() as $k) {
            echo '<option value="'.$k->id.'">
                 '.$k->id_polis_induk.' : '.$k->id_polis_detail.'
                 </option>';
        }
    }

	public function getheader()
	{
		$getheader = $this->mbulkpurchase->getheader(
            $this->input->post("asuransi"),
            $this->input->post("produk_id")
        );

		$example='';
		$headerall='';
        foreach ($getheader->result() as $k) {
            $headerall = $k->field_mandatory_list."|".$k->field_list;
            $example = $k->example_mandatory."|".$k->example;
        }
        echo '<input type="hidden" id="headercsv" value="'.$headerall.'">';
        echo '<input type="hidden" id="examplecsv" value="'.$example.'">';
	}

	public function downloadcsv()
    {
        $header = $this->input->get('header');
        $content = $this->input->get('example');
        $output= $header.PHP_EOL.$content;
        header("Content-type: application/txt");
        header('Content-Disposition: attachment; filename="TEMPLATE_BULK_PURCHASE_'.trim($this->input->get('produk')).'_'.trim($this->input->get('asuransi')).'.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        fputs($handle,$output);
        fclose($handle);
        exit;
    }

    public function ajax_data()
    {
        $page = $this->input->get('page');
        $produk = $this->input->get('produk');
        $asuransi = $this->input->get('asuransi');
        $status = $this->input->get('status');
        $id_purchase = $this->input->get('id_purchase');
        $search = addslashes($this->input->get('search'));
        $produk_id = $this->input->get('produk');
        if($page == ''){
            $page = 1;
        }
        if($search!= ''){
            $search = 'AND `data` LIKE \'%'.$search.'%\' OR `status` LIKE \'%'.$search.'%\' ';
        }
        $limit = 10;
        $offset = ($page-1)*$limit;

        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }else{
            return $polis_tb;
        }

        $data['getdata']= $this->mbulkpurchase->get($offset,$limit,$search,$produk,$asuransi,$status,$id_purchase,$polis_tb);
        $rows = array();
        foreach ($data['getdata']->result() as $row) {
            $row->viewform ="<button class='btn btn-success btn-flat' id='viewform-".$row->id_v."'><i class='fa fa-clipboard'></i> Lihat</button>
                            <script>
                                $(document).on('click', '#viewform-".$row->id_v."', function(e) {
                                    viewform(
                                        '".$row->namapeserta."',
                                        '".$row->tanggallahir."',
                                        '".$row->usia."',
                                        '".$row->mp_bulan."',
                                        '".$row->mulaiperjanjian."',
                                        '".$row->akhirperjanjian."',
                                        '".$row->manfaatasuransiawal."',
                                        '".$row->kontribusi."',
                                        '".$row->ketentuan_u."'                                        
                                    );
                                });
                            </script>
							";

			// additionalform
			$row->ruleadd = "<span><strong>".$row->ketentuan_u."</strong></span>";
            
            // $polisadd = $this->mbulkpurchase->addformdata($row->purchase_id, $row->id_v)->result();
            $polisadd = $this->mbulkpurchase->addformdata($row->purchase_id, $row->id_member);
            foreach($polisadd->result() as $p){

                $data_listadd[] = '<li><a href = '.site_url().'assets/upload/additionalform/'.$p->filename.'>'.$p->filename.'</li>'; 
                $row->listadd = implode('',$data_listadd);
            }

            $getaddformdata = $this->mbulkpurchase->addformdata(
                $id_purchase = $row->purchase_id,
                $id_member = $row->id_member
            );

            foreach ($getaddformdata->result() as $a) {
                if($a->user_upload_form == 'BELUM DIUPLOAD MEMBER'){
                    continue;
                }else{
                    $data_addform_u[] = '
                        <li><a href="'.site_url().'/assets/upload/additionalform/'.$a->user_upload_form.'" target="_blank">'.$a->user_upload_form.'</li>
                        ';
                    $row->addform_u = implode('',$data_addform_u);
                }
            }
            
            $tanggal_disetujui = $row->tanggal_disetujui == "0000-00-00" ? "" : $row->tanggal_disetujui;
            $row->tgldisetujui = '
                <input type="text" data-date-format="yyyy-mm-dd" class="form-control tgldisetujui" name="tgldisetujui[]" value="'.$tanggal_disetujui.'">
                <script>
                $(document).ready(function() {
                    $(".tgldisetujui").datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        dateFormat: "yy-mm-dd"
                    });
                });
                </script>
                ';
            if( $status == 'OPEN' && ($this->session->userdata('usergroup')=='2'|| $this->session->userdata('usergroup')=='8') ){
                $selected1='';
                $selected2='';
                $selected3='';

                $selectedM='';
                $selectedF='';

                if($row->markedas == 'APPROVE'){
                    $selected1='selected';
                }
                elseif($row->markedas == 'PENDING'){
                    $selected2='selected';
                }
                elseif($row->markedas == 'CANCEL'){
                    $selected3='selected';
                }

                $row->choose = '
                                <input type="text" name="id_v[]" class="id_v" value="'.$row->id_v.'" hidden>
                                <select class ="form-control choicemarked" id="choice-'.$row->id_v.'">
                                    <option value="-">TANDAI SEBAGAI</option>
                                    <option value="APPROVE" '.$selected1.'>APPROVE</option>
                                    <option value="PENDING" '.$selected2.'>PENDING</option>
                                    <option value="CANCEL" '.$selected3.'>CANCEL</option>
                                </select>
                                <script>
                                $(document).on("change", "#choice-'.$row->id_v.'", function(e) {
                                    checkedchoice( $(this).val(), "'.$row->id_v.'", "'.$row->purchase_id.'");
                                });
                                </script>
                                ';

                if($row->markedplus == 'F'){
                    $selectedF='selected';
                }
                elseif($row->markedplus == 'M'){
                    $selectedM='selected';
                }

                $row->addform ="
                                <button class='btn btn-success btn-flat' id='viewaddform-".$row->id_v."'><i class='fa fa-file'></i> Form Tambahan</button>
                                 
	                            <script>
	                                $(document).on('click', '#viewaddform-".$row->id_v."', function(e) {
	                                	$('#modaluseradd').modal('show');
	                                    $('#uaf_id_purchase').val('".$row->purchase_id."');
	                                    $('#uaf_id_member').val('".$row->id_member."');
	                                    getformtambahan('".$row->buyer."');
	                                    getaddform('".$row->purchase_id."','".$row->id_member."');
	                                });
	                            </script>
                            	";
            }elseif( $status == 'PENDING' && ($this->session->userdata('usergroup')=='2'|| $this->session->userdata('usergroup')=='8') ){
                $selected1='';
                $selected2='';
                $selected3='';

                if($row->markedas == 'APPROVE'){
                    $selected1='selected';
                }
                if($row->markedas == 'CANCEL'){
                    $selected3='selected';
                }

                $row->choose = '
                                <input type="text" name="id_v[]" value="'.$row->id_v.'" hidden>
                                <select class ="form-control choicemarked" id="choice-'.$row->id_v.'">
                                    <option value="-">TANDAI SEBAGAI</option>
                                    <option value="APPROVE" '.$selected1.'>APPROVE</option>
                                    <option value="CANCEL" '.$selected3.'>CANCEL</option>
                                </select>
                                <script>
                                $(document).on("change", "#choice-'.$row->id_v.'", function(e) {
                                    checkedchoice( $(this).val(), "'.$row->id_v.'", "'.$row->purchase_id.'");
                                });
                                </script>
                                ';
                $row->addform = "
                            	<button class='btn btn-success btn-flat' id='viewaddform-".$row->id_v."'><i class='fa fa-file'></i> Form Tambahan</button>
	                            <script>
	                                $(document).on('click', '#viewaddform-".$row->id_v."', function(e) {
	                                	$('#modaluseradd').modal('show');
	                                    $('#uaf_id_purchase').val('".$row->purchase_id."');
	                                    $('#uaf_id_member').val('".$row->id_member."');
	                                    getformtambahan('".$row->buyer."');
	                                    getaddform('".$row->purchase_id."','".$row->id_member."');
	                                });
	                            </script>
                                ";

            }elseif($status=='OPEN' && ($this->session->userdata('usergroup')=='1') ){
                $row->choose = '
                        <input type="text" name="id_v[]" class="id_v" value="'.$row->id_v.'" hidden>
                        <span class="text-blue"><i class="fa fa-clipboard"></i> Menunggu Diproses Broker </span>
                        ';
                // $row->addform = '<span class="text-blue"><i class="fa fa-clipboard"></i> Menunggu Diproses Broker </span>';
				$row->addform = "
                				<button class='btn btn-flat btn-info' id='viewmemberaddform-".$row->id_v."'> UPLOAD FORM TAMBAHAN</button>
                				<script>
	                                $(document).on('click', '#viewmemberaddform-".$row->id_v."', function(e) {
	                                	$('#maf_id_purchase').val('".$row->purchase_id."');
	                                    $('#maf_id_member').val('".$row->id_member."');
	                                    $('#modalmemberadd').modal('show');
	                                    getaddform('".$row->purchase_id."','".$row->id_member."');
	                                    getmemberaddform('".$row->purchase_id."','".$row->id_member."');
	                                });
	                            </script>

                                ";
            }elseif($status=='APPROVE'){
                $row->choose = '
                        <input type="text" name="id_v[]" class="id_v" value="'.$row->id_v.'" hidden>
                        <span class="text-green"> <i class="fa fa-check"></i> Sudah Disetujui Broker </span>
                    ';
                $row->addform = '<span class="text-green"><i class="fa fa-clipboard"></i> Auto cover </span>';
            }elseif($status=='CANCEL'){
                $row->choose = '
                        <input type="text" name="id_v[]" class="id_v" value="'.$row->id_v.'" hidden>
                        <span class="text-red"> <i class="fa fa-times"></i> Sudah Ditolak Broker </span>
                    ';
                // $row->addform = '<span class="text-red"><i class="fa fa-clipboard"></i> Tidak diperlukan </span>';
				$row->addform = "
                				<button class='btn btn-flat btn-info' id='viewmemberaddform-".$row->id_v."'> UPLOAD FORM TAMBAHAN</button>
                				<script>
	                                $(document).on('click', '#viewmemberaddform-".$row->id_v."', function(e) {
	                                	$('#maf_id_purchase').val('".$row->purchase_id."');
	                                    $('#maf_id_member').val('".$row->id_member."');
	                                    $('#modalmemberadd').modal('show');
	                                    getaddform('".$row->purchase_id."','".$row->id_member."');
	                                    getmemberaddform('".$row->purchase_id."','".$row->id_member."');
	                                });
	                            </script>
                                ";
            }elseif($status=='PENDING'){
                $row->addform = "
                				<button class='btn btn-flat btn-info' id='viewmemberaddform-".$row->id_v."'> UPLOAD FORM TAMBAHAN</button>
                				<script>
	                                $(document).on('click', '#viewmemberaddform-".$row->id_v."', function(e) {
	                                	$('#maf_id_purchase').val('".$row->purchase_id."');
	                                    $('#maf_id_member').val('".$row->id_member."');
	                                    $('#modalmemberadd').modal('show');
	                                    getaddform('".$row->purchase_id."','".$row->id_member."');
	                                    getmemberaddform('".$row->purchase_id."','".$row->id_member."');
	                                });
	                            </script>
                                ";
                $row->choose = '
                        <input type="text" name="id_v[]" class="id_v" value="'.$row->id_v.'" hidden>
                        <span class="text-yellow"><i class="fa fa-hand-paper-o"></i> Pemohon diharap mengisi form tambahan </span>
                        ';
            }
         $rows[] = array_values((array)$row);
        }
        $this->output->set_content_type('application/json');
        $data['ajax_data'] = json_encode(array('aaData'=> $rows));
        $this->load->view('ajax/ajax_data',$data);
    }

    public function ajax_data0()
    {
        $page = $this->input->get('page');
        $produk = $this->input->get('produk');
        $asuransi = $this->input->get('asuransi');
        $archive = $this->input->get('archive');
        //$status = $this->input->get('status');
        $search = addslashes($this->input->get('search'));
        if($page == ''){
            $page = 1;
        }
        if($search!= ''){
            $search = 'AND `a.id_purchase` LIKE \'%'.$search.'%\' OR d.`name` LIKE \'%'.$search.'%\' ';
        }
        $limit = 10;
        $offset = ($page-1)*$limit;

        $data['getdata']= $this->mbulkpurchase->get0($offset,$limit,$search,$produk,$asuransi,$archive);
        $rows = array();
        foreach ($data['getdata']->result() as $row) {
            $row->detail ="<button class='btn btn-success btn-flat' id='".$row->id_purchase."'><i class='fa fa-clipboard'></i> Lihat</button>
                            <script>
                                $(document).on('click', '#".$row->id_purchase."', function(e) {
                                    opendetaildata('".trim($row->id_purchase)."');
                                });
                            </script>";
            $row->upload = "
            				<button class='btn btn-success btn-flat' id='s-".$row->id_purchase."'><i class='fa fa-clipboard'></i> Upload PDF</button>
                            <script>
                                $(document).on('click', '#s-".$row->id_purchase."', function(e) {
                                    $('#sm_id_purchase').val('".$row->id_purchase."');
                                    $('#modalsertifikat').modal('show');
                                    getsertifikat('".$row->id_purchase."');
                                });
                            </script>
		                  	";

           $row->generatesertifikat = "<button class='btn btn-info btn-flat' id='g-".$row->id_purchase."-dw'><i class='fa fa-file'></i> Generate</button>
                            <script>
                                $(document).on('click', '#g-".$row->id_purchase."-dw', function(e) {
                                    // window.open('generatepdf?id_purchase=".$row->id_purchase."&nopolis=".$row->id_polis_induk."&nodetail=".$row->id_polis_detail."','_blank');
                                    $('#modalselectmember_sertifikat').modal('show');
                                    getmember_sertifikat('".$row->id_purchase."', '".$row->id_polis_induk."', '".$row->id_polis_detail."');
                                });
                            </script>";
                        
           $row->generatelampiran = "<button class='btn btn-info btn-flat' id='l-".$row->id_purchase."-dw'><i class='fa fa-file'></i> Generate</button>
                            <script>
                                $(document).on('click', '#l-".$row->id_purchase."-dw', function(e) {
                                    // window.open('generatepdflampiran?id_purchase=".$row->id_purchase."&nopolis=".$row->id_polis_induk."&nodetail=".$row->id_polis_detail."');
                                    $('#modalselectmember_lampiran').modal('show');
                                    getmember_lampiran('".$row->id_purchase."', '".$row->id_polis_induk."', '".$row->id_polis_detail."');
                                });
                            </script>";

		   $row->download="<button class='btn btn-success btn-flat' id='s-".$row->id_purchase."-dw'><i class='fa fa-clipboard'></i> Open Document</button>
                            <script>
                                $(document).on('click', '#s-".$row->id_purchase."-dw', function(e) {
                                   	$('#modalsertifikatdw').modal('show');
                                    getsertifikat('".$row->id_purchase."');
                                });
                            </script>";
            
            $var_archive = $row->archivestatus == 'OPEN' ? 'Arsipkan' : 'Buka Arsip';
            $row->archive="<button class='btn btn-primary btn-flat' id='archive-".$row->id_purchase."'><i class='fa fa-clipboard'></i> $var_archive</button>
                            <script>
                                $(document).on('click', '#archive-".$row->id_purchase."', function(e) {
                                    arsipkan('".$row->id_purchase."');
                                });
                            </script>";

            $rows[] = array_values((array)$row);
        }
        $this->output->set_content_type('application/json');
        $data['ajax_data'] = json_encode(array('aaData'=> $rows));
        $this->load->view('ajax/ajax_data',$data);
    }


    function paging()
    {
        $search = addslashes($this->input->get('search'));
        $asuransi = $this->input->get('asuransi');
        $produk = $this->input->get('produk');
        $status = $this->input->get('status');
        $id_purchase = $this->input->get('id_purchase');

        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->get('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }
        // if($search!= ''){
        //     $search = 'AND a.id_field LIKE \'%'.$search.'%\' OR a.caption LIKE \'%'.$search.'%\'  ';
        // }
        $getpage = $this->mbulkpurchase->getpage($search,$produk,$asuransi,$status,$id_purchase,$polis_tb);
        $countpage = 1;
        $limit = 10;
            foreach ($getpage->result() as $row) {
              $countpage = ceil(intval($row->page)/$limit);
            }
            if($countpage>0){
                echo '<input type="hidden" id="countpage" value="'.$countpage.'">';
                echo '<button class="btn btn-flat btn-info page1"><i class="fa fa-fast-backward"></i></button>';
                for ($i=1; $i <= $countpage ; $i++) {
                  echo '<button class="btn btn-flat btn-info page'.$i.'"">'.$i.'</button>';
                }
                echo '<button class="btn btn-flat btn-info page'.$countpage.'"><i class="fa fa-fast-forward"></i></button>';
            }else{
                echo '0';
            }
    }

    function paging0()
    {
        $search = addslashes($this->input->get('search'));
        $asuransi = $this->input->get('asuransi');
        $produk = $this->input->get('produk');
        $archive = $this->input->get('archive');
        //$status = $this->input->get('status');
        // if($search!= ''){
        //     $search = 'AND a.id_field LIKE \'%'.$search.'%\' OR a.caption LIKE \'%'.$search.'%\'  ';
        // }
        $getpage0 = $this->mbulkpurchase->getpage0($search,$produk,$asuransi,$archive);
        $countpage = 1;
        $limit = 10;
        // die(var_dump($getpage0->row()->page));
            foreach ($getpage0->result() as $row) {
              $countpage = ceil(intval($row->page)/$limit);
            }
            if($countpage>0){
                echo '<input type="hidden" id="countpage0" value="'.$countpage.'">';
                echo '<button class="btn btn-flat btn-info page01"><i class="fa fa-fast-backward"></i></button>';
                for ($i=1; $i <= $countpage ; $i++) {
                  echo '<button class="btn btn-flat btn-info page0'.$i.'"">'.$i.'</button>';
                }
                echo '<button class="btn btn-flat btn-info page0'.$countpage.'"><i class="fa fa-fast-forward"></i></button>';
            }else{
                echo '0';
            }
    }

    public function uploadsertified()
    {
    	$id_purchase = $this->input->post('id_purchase');
    	$sm_desc = $this->input->post('sm_desc');
    	$filename = 'S-'.$id_purchase.'-'.date('Ymdhis').'.pdf';
            $config['upload_path']          = './assets/upload/sertifikat/';
    		$config['allowed_types']        = 'pdf';
    		$config['overwrite']            = TRUE;
    		$config['max_size']             = 100000;
    		$config['file_name']            = $filename;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload()){
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
            }
            else{
            	echo $this->mbulkpurchase->updatesertifikat($id_purchase,$filename,$sm_desc);
            }
    }

    public function uploaddata()
    {
        $session_baru = array(
            'asid' => $this->input->post('maodalasuransi'),
            'prodid' => $this->input->post('modalproduk'),
        );

        $this->session->set_userdata($session_baru);
        
        $config['upload_path']          = './assets/upload/polis/';
        $config['allowed_types']        = 'xlsx|xls';
        $config['overwrite']            = TRUE;
        $config['max_size']             = 100000;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload()){
            $error = array('error' => $this->upload->display_errors());
            echo "gagal";
        }else{
            $this->load->library('excel');
            $fileName = $_FILES["userfile"]["tmp_name"];

            if ($_FILES["userfile"]["size"] > 0) {
                //insert ke tabel purchase queue dan polis purchase
                $insertqueue = $this->mbulkpurchase->insertqueue();
                $purchase_id = 'PN-'.str_pad($insertqueue, 6, '0', STR_PAD_LEFT);
                $insertpurchase = $this->mbulkpurchase->insertpurchase(
                    $this->input->post('id_asuransi'),
                    $this->input->post('id_produk'),
                    $purchase_id,
                    $this->input->post('id_polis'),
                    $this->input->post('namapengajuan')
                );
                $file = fopen($fileName, "r");
                
                $data_polismuaman = array(
                    'id_asuransi' => $this->input->post('id_asuransi'),
                    'id_produk' => $this->input->post('id_produk'),
                    'status' => 'OPEN',
                    'datestamp' => date('Y-m-d'),
                    'timestamp' => date('H:i:s'),
                    'purchase_type' => 'BULK',
                    'purchase_id' => $purchase_id,
                    'email' => $this->input->post('email'),
                    'buyer' => $this->session->userdata('userid'),
                    'last_update' => date('Y-m-d H:i:s')
                );
                $insert_polismuaman = $this->mbulkpurchase->insert_polismuaman($data_polismuaman);

                $getprodukid = $this->mbulkpurchase->getprodukid($this->input->post('id_polis'));
                foreach ($getprodukid->result() as $g) {
                    $produk_id = $g->produk_id;
                }
                
                if($produk_id == 24){ //if produk plus menurun
                    $polis_tb = 'polis_muaman_plusmenurun';
                    $this->load->library('excel');
                    $fileName = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($fileName);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $namapeserta_title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            if (!is_int($namapeserta_title)) {
                                $polismenurun_upload[] = $namapeserta_title;
                            }
                        }
                    }
                    $path = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 1; $row <= $highestRow; $row++) {
                            $no = $worksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue();
                            $namapeserta = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $tanggallahir = $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue();
                            $usia = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
                            $mulaiperjanjian = $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
                            $mp_bulan = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $akhirperjanjian = $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();; 
                            $nd = $worksheet->getCellByColumnAndRow(7, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(7, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(7, $row)->getvalue();
                            $pa = $worksheet->getCellByColumnAndRow(8, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(8, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(8, $row)->getvalue();
                            $phk = $worksheet->getCellByColumnAndRow(9, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(9, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(9, $row)->getvalue();
                            $bi = $worksheet->getCellByColumnAndRow(10, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(10, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(10, $row)->getvalue();
                            $kpp = $worksheet->getCellByColumnAndRow(11, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(11, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(11, $row)->getvalue();
                            $nd_number = $worksheet->getCellByColumnAndRow(12, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(12, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(12, $row)->getvalue();
                            $pa_number = $worksheet->getCellByColumnAndRow(13, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(13, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(13, $row)->getvalue();
                            $phk_number = $worksheet->getCellByColumnAndRow(14, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(14, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(14, $row)->getvalue();
                            $bi_number = $worksheet->getCellByColumnAndRow(15, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(15, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(15, $row)->getvalue();
                            $kpp_number = $worksheet->getCellByColumnAndRow(16, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(16, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(16, $row)->getvalue();
                            $total = $worksheet->getCellByColumnAndRow(17, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(17, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(17, $row)->getvalue();
                            $manfaatasuransiawal = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                            $kontribusi = $worksheet->getCellByColumnAndRow(19, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(19, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(19, $row)->getvalue();
                            $ketentuan_u = $worksheet->getCellByColumnAndRow(20, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(20, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(20, $row)->getvalue();
                            if($usia != 'Usia' && str_replace(",","",$manfaatasuransiawal) >= 1000 && $namapeserta != null && $tanggallahir != null){
                                $uniqid = uniqid();
                                $data[] = array(
                                    'id_polis_muaman' => $insert_polismuaman,
                                    'id_member' => $no.'-'.$uniqid,
                                    'namapeserta' => $namapeserta,
                                    'tanggallahir' => $tanggallahir,
                                    'usia' => $usia,
                                    'mulaiperjanjian' => $mulaiperjanjian,
                                    'mp_bulan' => $mp_bulan,
                                    'akhirperjanjian' => $akhirperjanjian,
                                    'nd' => $nd,
                                    'pa' => $pa,
                                    'phk' => $phk,
                                    'bi' => $bi,
                                    'kpp' => $kpp,
                                    'nd_number' => $nd_number,
                                    'pa_number' => $pa_number,
                                    'phk_number' => $phk_number,
                                    'bi_number' => $bi_number,
                                    'kpp_number' => $kpp_number,
                                    'total' => str_replace(",","",$total),
                                    'manfaatasuransiawal' => str_replace(",","",$manfaatasuransiawal),
                                    'kontribusi' => str_replace(",","",$kontribusi),
                                    'ketentuan_u' => $ketentuan_u,
                                    'last_update' => date('Y-m-d H:i:s')
                                );
                                $data_member[] = array(
                                    'member_id' => $no.'-'.$uniqid,
                                    'member_name' => $namapeserta,
                                    'member_tempat_lahir' => 'jakarta(hc)',
                                    'member_tanggal_lahir' => $tanggallahir,
                                    'member_jenis_kelamin' => 'laki-laki(hc)',
                                    'id_nasabah' => $this->session->userdata('userid'), 
                                    'datestamp' => date('Y-m-d'),
                                    'timestamo' => date('H:i:s')
                                );
                            }else{
                                continue;
                            }
                        }
                    }                    
                    $insert_excel = $this->mbulkpurchase->insert_excel($data,$polis_tb);
                    if($insert_excel){
                        $insertdata_member = $this->mbulkpurchase->insertdata_member($data_member);
                        if($insertdata_member){
                            return TRUE;
                        }else{
                            echo 'gagal';
                        }
                    }else{
                       echo 'gagal';
                    }

                
                }elseif($produk_id == 25){ //if produk menurun
                    $polis_tb = 'polis_muaman_menurun';
                    $this->load->library('excel');
                    $fileName = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($fileName);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $namapeserta_title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            if (!is_int($namapeserta_title)) {
                                $polismenurun_upload[] = $namapeserta_title;
                            }
                        }
                    }
                    $path = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 1; $row <= $highestRow; $row++) {
                            $no = $worksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue();
                            $namapeserta = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $tanggallahir = $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue();
                            $usia = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
                            $mulaiperjanjian = $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
                            $mp_bulan = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $akhirperjanjian = $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();
                            $tarif = $worksheet->getCellByColumnAndRow(7, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(7, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $manfaatasuransiawal = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $kontribusi = $worksheet->getCellByColumnAndRow(9, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(9, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(9, $row)->getvalue();
                            $ketentuan_u = $worksheet->getCellByColumnAndRow(10, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(10, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(10, $row)->getvalue();
                            if($usia != 'Usia' && str_replace(",","",$manfaatasuransiawal) >= 1000 && $namapeserta != null && $tanggallahir != null){
                                $uniqid = uniqid();
                                $data[] = array(
                                    'id_polis_muaman' => $insert_polismuaman,
                                    'id_member' => $no.'-'.$uniqid,
                                    'namapeserta' => $namapeserta,
                                    'tanggallahir' => $tanggallahir,
                                    'usia' => $usia,
                                    'mulaiperjanjian' => $mulaiperjanjian,
                                    'mp_bulan' => $mp_bulan,
                                    'akhirperjanjian' => $akhirperjanjian,
                                    'tarif' => str_replace(",","",$tarif),
                                    'manfaatasuransiawal' => str_replace(",","",$manfaatasuransiawal),
                                    'kontribusi' => str_replace(",","",$kontribusi),
                                    'ketentuan_u' => $ketentuan_u,
                                    'last_update' => date('Y-m-d H:i:s')
                                );
                                $data_member[] = array(
                                    'member_id' => $no.'-'.$uniqid,
                                    'member_name' => $namapeserta,
                                    'member_tempat_lahir' => 'jakarta(hc)',
                                    'member_tanggal_lahir' => $tanggallahir,
                                    'member_jenis_kelamin' => 'laki-laki(hc)',
                                    'id_nasabah' => $this->session->userdata('userid'), 
                                    'datestamp' => date('Y-m-d'),
                                    'timestamo' => date('H:i:s')
                                );
                            }else{
                                continue;
                            }
                        }
                    }                    
                    $insert_excel = $this->mbulkpurchase->insert_excel($data,$polis_tb);
                    if($insert_excel){
                        $insertdata_member = $this->mbulkpurchase->insertdata_member($data_member);
                        if($insertdata_member){
                            return TRUE;
                        }else{
                            echo 'gagal';
                        }
                    }else{
                       echo 'gagal';
                    }
                
                }elseif($produk_id == 23){ //if produk tetap
                    $polis_tb = 'polis_muaman_tetap';
                    $this->load->library('excel');
                    $fileName = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($fileName);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $namapeserta_title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            if (!is_int($namapeserta_title)) {
                                $polismenurun_upload[] = $namapeserta_title;
                            }
                        }
                    }
                    $path = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 1; $row <= $highestRow; $row++) {
                            $no = $worksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue();
                            $namapeserta = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $tanggallahir = $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue();
                            $usia = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
                            $mulaiperjanjian = $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
                            $mp_bulan = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $akhirperjanjian = $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();
                            $tarif = $worksheet->getCellByColumnAndRow(7, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(7, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $manfaatasuransiawal = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $kontribusi = $worksheet->getCellByColumnAndRow(9, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(9, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(9, $row)->getvalue();
                            $ketentuan_u = $worksheet->getCellByColumnAndRow(10, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(10, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(10, $row)->getvalue();
                            if($usia != 'Usia' && str_replace(",","",$manfaatasuransiawal) >= 1000 && $namapeserta != null && $tanggallahir != null){
                                $uniqid = uniqid();
                                $data[] = array(
                                    'id_polis_muaman' => $insert_polismuaman,
                                    'id_member' => $no.'-'.$uniqid,
                                    'namapeserta' => $namapeserta,
                                    'tanggallahir' => $tanggallahir,
                                    'usia' => $usia,
                                    'mulaiperjanjian' => $mulaiperjanjian,
                                    'mp_bulan' => $mp_bulan,
                                    'akhirperjanjian' => $akhirperjanjian,
                                    'tarif' => str_replace(",","",$tarif),
                                    'manfaatasuransiawal' => str_replace(",","",$manfaatasuransiawal),
                                    'kontribusi' => str_replace(",","",$kontribusi),
                                    'ketentuan_u' => $ketentuan_u,
                                    'last_update' => date('Y-m-d H:i:s')
                                );
                                $data_member[] = array(
                                    'member_id' => $no.'-'.$uniqid,
                                    'member_name' => $namapeserta,
                                    'member_tempat_lahir' => 'jakarta(hc)',
                                    'member_tanggal_lahir' => $tanggallahir,
                                    'member_jenis_kelamin' => 'laki-laki(hc)',
                                    'id_nasabah' => $this->session->userdata('userid'), 
                                    'datestamp' => date('Y-m-d'),
                                    'timestamo' => date('H:i:s')
                                );
                            }else{
                                continue;
                            }
                        }
                    }                    
                    $insert_excel = $this->mbulkpurchase->insert_excel($data,$polis_tb);
                    if($insert_excel){
                        $insertdata_member = $this->mbulkpurchase->insertdata_member($data_member);
                        if($insertdata_member){
                            return TRUE;
                        }else{
                            echo 'gagal';
                        }
                    }else{
                       echo 'gagal';
                    }
                }elseif($produk_id == 27){ //if produk jma pembiayaan
                    $polis_tb = 'polis_muaman_jma_pembiayaan';
                    $this->load->library('excel');
                    $fileName = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($fileName);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $namapeserta_title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            if (!is_int($namapeserta_title)) {
                                $polismenurun_upload[] = $namapeserta_title;
                            }
                        }
                    }
                    $path = $_FILES["userfile"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach($object->getWorksheetIterator() as $worksheet){
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumn++;
                        for ($row = 1; $row <= $highestRow; $row++) {
                            $no = $worksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue();
                            $namapeserta = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            //column C di excelnya kosong
                            $tanggallahir = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
                            $jeniskelamin = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $manfaatasuransiawal = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $usia = $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();
                            //note test//
                            $masaasuransi_bulan = $worksheet->getCellByColumnAndRow(7, $row)->getOldCalculatedValue();
                            $mulai = $worksheet->getCellByColumnAndRow(8, $row)->getFormattedValue();
                            $sampai = $worksheet->getCellByColumnAndRow(9, $row)->getFormattedValue();
                            $rate_manfaatasuransiawal = $worksheet->getCellByColumnAndRow(10, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(10, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(10, $row)->getvalue();
                            $kontribusi_sekaligus = $worksheet->getCellByColumnAndRow(11, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(11, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(11, $row)->getvalue();
                            $keterangan = $worksheet->getCellByColumnAndRow(12, $row)->isFormula() ? $worksheet->getCellByColumnAndRow(12, $row)->getOldCalculatedValue() : $worksheet->getCellByColumnAndRow(12, $row)->getvalue();
                            if($usia != 'Usia' && str_replace(",","",$manfaatasuransiawal) >= 1000 && $namapeserta != null && $tanggallahir != null && $jeniskelamin != null && $masaasuransi_bulan != null && $mulai != null && $sampai != null){
                                $uniqid = uniqid();
                                $data[] = array(
                                    'id_polis_muaman' => $insert_polismuaman,
                                    'id_member' => $no.'-'.$uniqid,
                                    'namapeserta' => $namapeserta,
                                    'tanggallahir' => $tanggallahir,
                                    'jeniskelamin' => $jeniskelamin,
                                    'manfaatasuransiawal' => str_replace(",","",$manfaatasuransiawal),
                                    'usia' => $usia,
                                    'mp_bulan' => $masaasuransi_bulan,
                                    'mulaiperjanjian' => $mulai,
                                    'akhirperjanjian'=> $sampai,
                                    'rate_manfaatasuransiawal' => $rate_manfaatasuransiawal,
                                    'kontribusi' => str_replace(",","",$kontribusi_sekaligus),
                                    'ketentuan_u' => $keterangan,
                                    'last_update' => date('Y-m-d H:i:s')
                                );
                                $data_member[] = array(
                                    'member_id' => $no.'-'.$uniqid,
                                    'member_name' => $namapeserta,
                                    'member_tempat_lahir' => 'jakarta(hc)',
                                    'member_tanggal_lahir' => $tanggallahir,
                                    'member_jenis_kelamin' => $jeniskelamin,
                                    'id_nasabah' => $this->session->userdata('userid'), 
                                    'datestamp' => date('Y-m-d'),
                                    'timestamo' => date('H:i:s')
                                );
                            }else{
                                continue;
                            }
                        }
                    }                    
                    $insert_excel = $this->mbulkpurchase->insert_excel($data,$polis_tb);
                    if($insert_excel){
                        $insertdata_member = $this->mbulkpurchase->insertdata_member($data_member);
                        if($insertdata_member){
                            return TRUE;
                        }else{
                            echo 'gagal';
                        }
                    }else{
                       echo 'gagal';
                    }
                } 

            }else{
                echo "gagal";
            }
        }
    }

    public function updateflag()
    {
        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->post('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }

        $update = $this->mbulkpurchase->updateflag(
            $this->input->post('flag'),
            $this->input->post('id_v'),
            $polis_tb
        );
    }

    public function addformtype()
    {
        $update = $this->mbulkpurchase->addform(
            $this->input->post('addform'),
            $this->input->post('id')
        );
    }

    public function updatestatusflag()
    {
        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->post('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }

        $update = $this->mbulkpurchase->updatestatusflag(
            $this->input->post('id_purchase'),
            $polis_tb,
            $this->input->post('id_v'),
            $this->input->post('tgldisetujui')
            );
        // if($update){
        //     $getemail = $this->mbulkpurchase->getemaildata(
        //         $this->input->post('id_purchase')
        //     );
        //     foreach ($getemail->result() as $k) {
        //         $email = $k->email;
        //         $status = $k->status;
        //         $proses = $this->sendemail($email, $status, $this->input->post('id_purchase'));
        //         if($proses==1){
        //             $setmarkednull = $this->mbulkpurchase->setmarkednull(
        //                 $status,
        //                 $this->input->post('id_purchase')
        //             );
        //         }
        //     }
        // }
    }

    public function updateflagall()
    {
        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->post('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }

        $update = $this->mbulkpurchase->updateflagall(
            $this->input->post('flag'),
            $this->input->post('where'),
            $this->input->post('id_purchase'),
            $polis_tb
        );
    }

    function sendemail($email, $status,$id_purchase)
    {
       //$email_attach = '';
        $email_tujuan = $email;
        $email_subject = 'STATUS - '.$status;
        $email_cc = $this->input->post('email_cc');
        $email_attach = $this->input->post('email_attachment');

        if($status=='APPROVE'){
            $keterangan = 'Pengajuan Polis Disetujui';
        }elseif($status=='PENDING'){
            $keterangan = 'Pengajuan Polis Ditunda dan perlu mengisi form tambahan';
        }elseif($status=='CANCEL'){
            $keterangan = 'Pengajuan Polis Ditolak';
        }

        $email_content = '<table style="background-color:#000"; border="0">';
        $a=1;
        $getdatastatus = $this->mbulkpurchase->getdatastatus($status,$id_purchase);
        foreach ($getdatastatus->result() as $k) {
           if($a==1){
                $header = '<tr style="background-color:#FFF"><td>'.str_replace("|", "</td><td>", $k->headerform).'</td><td>STATUS</td><td>KETERANGAN</td></tr>';
                $content = '<tr style="background-color:#FFF"><td>'.str_replace("|", "</td><td>", $k->data).'</td><td>'.$status.'</td><td>'.$keterangan.'</td></tr>';
                $email_content = $email_content.$header.$content;
           }else{
                $content = '<tr style="background-color:#FFF"><td>'.str_replace("|", "</td><td>", $k->data).'</td><td>'.$status.'</td><td>'.$keterangan.'</td></tr>';
                $email_content = $email_content.$content;
           }
           $a++;
        }


        $email_content = $email_content.'</table>';

        $config = Array(
            // 'protocol' => 'smtp',
            // 'smtp_host' => 'ssl://tiramisu.qwords.net',
            // 'smtp_port' => 465,
            // 'smtp_user' => 'reportfif@gmail.com', // change it to yours
            // 'smtp_pass' => '@PKP123456', // change it to yours
            // 'mailtype' => 'html',
            // 'charset' => 'iso-8859-1',
            // 'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('b2b.fif.sim@gmail.com', 'MUAMAN TEST');
        //$this->email->to('{$email_tujuan}');
        $this->email->to("$email_tujuan");
        //$this->email->cc('dev-cc@pkp.co.id, sd-cc@pkp.co.id');
        //$this->email->cc('');
        $this->email->bcc('');

        $timestamp = date('d.m.Y H:i');

        $this->email->subject("$email_subject");

        $data['content'] = $email_content;
        $body = $this->load->view('email/content_email', $data, TRUE);

        $this->email->message($body);
        $this->email->attach("$email_attach");

        //$this->email->send();
        //echo $this->email->print_debugger();
        if($this->email->send())
        {
            return 1;
        }else{
            show_error($this->email->print_debugger());
            return 0;
        }
    }

    public function queryrule($ruleid,$fields,$where)
    {
        $rule_field = explode("|",$fields);
        $temptbl='';
        $join ='';
        $on = '';
        $select = '';
        $concat ='';
        for ($i=0; $i < count($rule_field) ; $i++) {
            if($i>0){
                $on = " ON t0.condition_order = t".$i.".condition_order";
            }
            $temptbl = $temptbl.$join."
                        (SELECT
                        `value` `".$rule_field[$i]."`,condition_order,formadditional,result,rule_id FROM apm_rule_condition
                        WHERE fieldname = '".$rule_field[$i]."') t".$i."
                        ".$on;

            $join = " LEFT JOIN ";

        }
        $query = "select t0.result, b.formname, b.id formid FROM ".$temptbl."
                  LEFT JOIN apm_additional_form b
                  ON
                  t0.formadditional = b.id
                  where t0.rule_id =".$ruleid." ".$where;

        echo $query;
        $getresult= $this->mbulkpurchase->getresult($query);
        return $getresult;
    }

    public function viewsertifikat()
   	{
   		$getsertifikat = $this->mbulkpurchase->getsertifikat(
   			$id_purchase = $this->input->post('id_purchase')
   		);

   		foreach ($getsertifikat->result() as $a) {
   			echo '
   			<tr>
   				<td><a href="'.site_url().'/assets/upload/sertifikat/'.$a->sertifikat.'" target="_blank"><button class="btn btn-info btn-flat"><i class="fa fa-file"></i>'.$a->sertifikat.'</button></a></td>
   				<td>'.$a->description.'</td>
   				<td>'.$a->date_upload.'</td>
   			</tr>
   			';
   		}
   	}

   	public function getformtambahan()
   	{
   		$getformtambahan = $this->mbulkpurchase->getformtambahan(
   			$this->input->post('id_asuransi'),
   			$this->input->post('id_produk'),
   			$this->input->post('id_user')
   		);

   		foreach ($getformtambahan->result() as $a) {
   			echo '
   			<option value="'.$a->id.'">'.$a->formname.'</option>
   			';
   		}
   	}

   	public function saveuseraddform()
   	{
   		$saveuseraddform = $this->mbulkpurchase->saveuseraddform(
   			$this->input->post('id_purchase'),
   			$this->input->post('id_member'),
   			$this->input->post('id_addform')
   		);

   		echo $saveuseraddform;
   	}

   	public function viewaddform()
   	{
   		$getaddformdata = $this->mbulkpurchase->addformdata(
   			$id_purchase = $this->input->post('id_purchase'),
   			$id_member = $this->input->post('id_member')
   		);

   		foreach ($getaddformdata->result() as $a) {
   			if ($a->user_upload_form != 'BELUM DIUPLOAD MEMBER' ) {
   				$a->user_upload_form = '<a href="'.site_url().'/assets/upload/additionalform/'.$a->user_upload_form.'" target="_blank"><button class="btn btn-info btn-flat"><i class="fa fa-file"></i> '.$a->user_upload_form.'</button></a>';
   			}
   			echo '
   			<tr>
   				<td>'.$a->formname.'</td>
   				<td><a href="'.site_url().'/assets/upload/additionalform/'.$a->filename.'" target="_blank"><button class="btn btn-info btn-flat"><i class="fa fa-file"></i> '.$a->filename.'</button></a></td>
   				<td>'.$a->user_upload_form.'</td>
   				<td>'.$a->date_upload.'</td>
   			</tr>
   			';
   		}
   	}

   	public function getmemberaddform()
   	{
   		$getaddformdata = $this->mbulkpurchase->addformdata(
   			$id_purchase = $this->input->post('id_purchase'),
   			$id_member = $this->input->post('id_member')
   		);

   		foreach ($getaddformdata->result() as $a) {
   			echo '
   			<option value="'.$a->additional_form_id.'">'.$a->formname.$a.'</option>
   			';
   		}
   	}

    public function uploadadditionalform()
    {
    	$id_purchase = $this->input->post('id_purchase');
    	$id_member = $this->input->post('id_member');
    	$id_addform = $this->input->post('id_addform');

		// change space with "_" (underscore)
    	$filename = str_replace(" ", "_",'AF-'.$id_purchase.'-'.$id_member.'-'.$id_addform.'.pdf');
            $config['upload_path']          = './assets/upload/additionalform/';
    		$config['allowed_types']        = 'pdf';
    		$config['overwrite']            = TRUE;
    		$config['max_size']             = 100000;
    		$config['file_name']            = $filename;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload()){
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
            }
            else{
            	echo $this->mbulkpurchase->updateformtambahan($id_purchase,$id_member,$id_addform,$filename);
            }
    }

    public function getmember_sertifikat(){
        //data untuk modalselectmember_lampiran
        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->post('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }

        $getpurchasedata = $this->mbulkpurchase->getpurchasepdf(
            $this->input->post('id_purchase'), 
            $polis_tb
        );
        
            echo '
                <input type="checkbox" id="select_all_sertifikat" name="select_all" style=""><b>&nbsp; Select All/Unselect All </b>
                <input type="text" name="id_purchase" hidden value="'.$this->input->post('id_purchase').'">
                <input type="text" name="nopolis" hidden value="'.$this->input->post('id_polis_induk').'">
                <input type="text" name="nodetail" hidden value="'.$this->input->post('id_polis_detail').'">
                <table class="table table-bordered table-striped table-hover row-border highlight">
                    <thead>        
                        <tr class="bg-blue">
                            <td><b>No.</b></td>           
                            <td><b>NAMA PESERTA</b></td>
                            <td><b>KETERANGAN</b></td>
                            <td><b>OPSI</b></td>
                        </tr>
                    </thead>';
                $i = 1;
                foreach($getpurchasedata->result() as $g){
                $no = $i++;
                echo '<tbody>
                        <td>'.$no.'</td>
                        <td>'.$g->namapeserta.'</td>
                        <td>'.$g->ketentuan_u.'</td>
                        <td><input type="checkbox" name="checkbox[]" class="checkbox_sertifikat" value="'.$g->id_member.'"><span> Pilih</span></td>
                    </tbody>
                ';
                }
            echo '</table>';
            echo '<script>
                $(".checkbox_sertifikat").change(function() {
                    if ($(".checkbox_sertifikat:checked").length) {
                        $("#btngeneratesertifikat").removeAttr("disabled");
                    } else {
                        $("#btngeneratesertifikat").attr("disabled", "disabled");
                    }
                    });
                $("#select_all_sertifikat").click(function() {
                    $("#btngeneratesertifikat").attr("disabled", !this.checked);
                    var c = this.checked;
                    $(".checkbox_sertifikat:checkbox").prop("checked",c);
                });
                </script>';
    }

    public function getmember_lampiran(){
        //data untuk modalselectmember_lampiran
        $get_produkid_bypurchase = $this->mbulkpurchase->get_produkid_bypurchase($this->input->post('id_purchase'));
        foreach($get_produkid_bypurchase->result() as $g){
            $produk_id = $g->id_produk;
        }
        $polis_tb = "";
        if($produk_id == 23){
            $polis_tb = 'polis_muaman_tetap';
        }elseif($produk_id == 24){
            $polis_tb = 'polis_muaman_plusmenurun';
        }elseif($produk_id == 25){
            $polis_tb = 'polis_muaman_menurun';
        }elseif($produk_id == 27){
            $polis_tb = 'polis_muaman_jma_pembiayaan';
        }

        $getpurchasedata = $this->mbulkpurchase->getpurchasepdf(
            $this->input->post('id_purchase'), 
            $polis_tb
        );
        
            echo '
                <input type="checkbox" id="select_all_lampiran" name="select_all" style=""><b>&nbsp; Select All/Unselect All </b>
                <input type="text" name="id_purchase" hidden value="'.$this->input->post('id_purchase').'">
                <input type="text" name="nopolis" hidden value="'.$this->input->post('id_polis_induk').'">
                <input type="text" name="nodetail" hidden value="'.$this->input->post('id_polis_detail').'">
                <table class="table table-bordered table-striped table-hover row-border highlight">
                    <thead>        
                        <tr class="bg-blue">
                            <td><b>No.</b></td>           
                            <td><b>NAMA PESERTA</b></td>
                            <td><b>KETERANGAN</b></td>
                            <td><b>OPSI</b></td>
                        </tr>
                    </thead>';
                $i = 1;
                foreach($getpurchasedata->result() as $g){
                $no = $i++;
                echo '<tbody>
                        <td>'.$no.'</td>        
                        <td>'.$g->namapeserta.'</td>
                        <td>'.$g->ketentuan_u.'</td>
                        <td><input type="checkbox" name="checkbox[]" class="checkbox_lampiran" value="'.$g->id_member.'"><span> Pilih</span></td>
                    </tbody>
                ';
                }
            echo '</table>';
            echo '<script>
                $(".checkbox_lampiran").change(function() {
                    if ($(".checkbox_lampiran:checked").length) {
                        $("#btngeneratelampiran").removeAttr("disabled");
                    } else {
                        $("#btngeneratelampiran").attr("disabled", "disabled");
                    }
                });
                
                $("#select_all_lampiran").click(function() {
                    $("#btngeneratelampiran").attr("disabled", !this.checked);
                    var c = this.checked;
                    $(".checkbox_lampiran:checkbox").prop("checked",c);
                });
            </script>';

    }
    
    public function arsipkan(){
        $arsipkan = $this->mbulkpurchase->arsipkan(
            $this->input->post('id_purchase'),
            $this->input->post('selectarchive')
        );

        echo $arsipkan;
    }
}

/* End of file c_dailytask.php */
/* Location: ./application/controllers/upload/c_dailytask.php */
