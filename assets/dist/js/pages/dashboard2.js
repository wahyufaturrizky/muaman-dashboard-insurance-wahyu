$(function () {

  'use strict';

  // ChartJS
  // -------
  // Here we will create a few charts using ChartJS

  //--------------------------------
  //- START PENGAJUAN HARIAN CHART -
  //--------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartPengajuanHarianCanvas = $("#chart-Pengajuan-Harian").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var PengajuanHarian = new Chart(chartPengajuanHarianCanvas);

  var PengajuanHarianData = {
    labels: ["Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"],
    datasets: [
      {
        label: "Pengajuan Arsip Harian",
        fillColor: "rgba(230, 204, 255, 0.5)",
        strokeColor: "rgba(230, 204, 255, 1)",
        pointColor: "rgba(230, 204, 255, 1)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56, 55, 40,]
      },
      {
        label: "Pengajuan Aktif Harian",
        fillColor: "rgba(0, 133, 72, 0.5)",
        strokeColor: "rgba(0, 133, 72, 1)",
        pointColor: "rgba(0, 133, 72, 1)",
        pointStrokeColor: "rgba(0, 133, 72, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(0, 133, 72, 1)",
        data: [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };

  var PengajuanHarianOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the Area chart
  PengajuanHarian.Line(PengajuanHarianData, PengajuanHarianOptions);

  //------------------------------
  //- END PENGAJUAN HARIAN CHART -
  //------------------------------



  //--------------------------------
  //- START PENGAJUAN BULANAN CHART -
  //--------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartPengajuanBulananCanvas = $("#chart-Pengajuan-Bulanan").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var PengajuanBulanan = new Chart(chartPengajuanBulananCanvas);

  var PengajuanBulananData = {
    labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    datasets: [
      {
        label: "Pengajuan Arsip Bulanan",
        fillColor: "rgba(230, 204, 255, 0.5)",
        strokeColor: "rgba(230, 204, 255, 1)",
        pointColor: "rgba(230, 204, 255, 1)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56, 55, 40, 40, 55, 40, 40, 90, ]
      },
      {
        label: "Pengajuan Arsip Bulanan",
        fillColor: "rgba(0, 133, 72, 0.5)",
        strokeColor: "rgba(0, 133, 72, 1)",
        pointColor: "rgba(0, 133, 72, 1)",
        pointStrokeColor: "rgba(0, 133, 72, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(0, 133, 72, 1)",
        data: [28, 48, 40, 19, 86, 27, 90, 70, 20, 80, 50, 80 ]
      }
    ]
  };

  var PengajuanBulananOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  PengajuanBulanan.Line(PengajuanBulananData, PengajuanBulananOptions);

  //-------------------------------
  //- END PENGAJUAN BULANAN CHART -
  //-------------------------------


  //--------------------------------
  //- START PENGAJUAN TAHUNAN CHART -
  //--------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartPengajuanTahunanCanvas = $("#chart-Pengajuan-Tahunan").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var PengajuanTahunan = new Chart(chartPengajuanTahunanCanvas);

  var PengajuanTahunanData = {
    labels: ["2015","2016","2017","2018" ,"2019"],
    datasets: [
      {
        label: "Pengajuan Arsip Tahunan",
        fillColor: "rgba(230, 204, 255, 0.5)",
        strokeColor: "rgba(230, 204, 255, 1)",
        pointColor: "rgba(230, 204, 255, 1)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56]
      },
      {
        label: "Pengajuan Arsip Tahunan",
        fillColor: "rgba(0, 133, 72, 0.5)",
        strokeColor: "rgba(0, 133, 72, 1)",
        pointColor: "rgba(0, 133, 72, 1)",
        pointStrokeColor: "rgba(0, 133, 72, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(0, 133, 72, 1)",
        data: [28, 48, 40, 19, 86]
      }
    ]
  };

  var PengajuanTahunanOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  PengajuanTahunan.Line(PengajuanTahunanData, PengajuanTahunanOptions);

  //-------------------------------
  //- END PENGAJUAN TAHUNAN CHART -
  //-------------------------------

  //-----------------------------------------------------------------
  //- START CHART AREA STATUS MEMBER OPEN, APPROVE, PENDING, CANCEL -
  //-----------------------------------------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartStatusMemberBulananCanvas = $("#chartAreaStatusMemberBulanan").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var StatusMemberBulanan = new Chart(chartStatusMemberBulananCanvas);

  var StatusMemberBulananData = {
    labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    datasets: [
      {
        label: "Member Open",
        fillColor: "rgba(0, 133, 72, 0.5)",
        strokeColor: "rgba(0, 133, 72, 1)",
        pointColor: "rgba(0, 133, 72, 1)",
        pointStrokeColor: "rgba(0, 133, 72, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(0, 133, 72, 1)",
        data: [10, 30, 60, 90, 60, 30, 20, 10, 5, 10, 60, 30 ]
      },
      {
        label: "Member Approve",
        fillColor: "rgba(46, 175, 244, 0.5)",
        strokeColor: "rgba(46, 175, 244, 1)",
        pointColor: "rgba(46, 175, 244, 1)",
        pointStrokeColor: "rgba(46, 175, 244, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(46, 175, 244, 1)",
        data: [30, 60, 10, 5, 10, 20, 30, 60, 90, 60, 30, 10 ]
      },
      {
        label: "Member Pending",
        fillColor: "rgba(249, 189, 99, 0.5)",
        strokeColor: "rgba(249, 189, 99, 1)",
        pointColor: "rgba(249, 189, 99, 1)",
        pointStrokeColor: "rgba(249, 189, 99, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(249, 189, 99, 1)",
        data: [40, 20, 20, 30, 50, 80, 90, 70, 20, 80, 50, 80 ]
      },
      {
        label: "Member Cancel",
        fillColor: "rgba(230, 204, 255, 0.5)",
        strokeColor: "rgba(230, 204, 255, 1)",
        pointColor: "rgba(230, 204, 255, 1)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56, 55, 40, 40, 55, 40, 40, 90, ]
      },
    ]
  };

  var StatusMemberBulananOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  StatusMemberBulanan.Bar(StatusMemberBulananData, StatusMemberBulananOptions);

  //---------------------------------------------------------------
  //- END CHART AREA STATUS MEMBER OPEN, APPROVE, PENDING, CANCEL -
  //---------------------------------------------------------------


  //--------------------------------
  //- START CHART LINE TOTAL PREMI -
  //--------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartPremiBulananCanvas = $("#chartLinePremiBulanan").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var PremiBulanan = new Chart(chartPremiBulananCanvas);

  var PremiBulananData = {
    labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    datasets: [
      {
        label: "Member Open",
        strokeColor: "rgba(46, 175, 244, 1)",
        fillColor: "rgba(255, 255, 255, 0)",
        pointColor: "rgba(46, 175, 244, 1)",
        pointStrokeColor: "rgba(46, 175, 244, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(46, 175, 244, 1)",
        data: [10, 30, 60, 90, 60, 30, 20, 10, 5, 10, 60, 30 ]
      }
    ]
  };

  var PremiBulananOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  PremiBulanan.Line(PremiBulananData, PremiBulananOptions);

  //------------------------------------
  //- END CHART CHART LINE TOTAL PREMI -
  //------------------------------------


  //-----------------------------------
  //- START CHART LINE TOTAL PINJAMAN -
  //-----------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var chartPinjamanBulananCanvas = $("#chartLinePinjamanBulanan").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var PinjamanBulanan = new Chart(chartPinjamanBulananCanvas);

  var PinjamanBulananData = {
    labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    datasets: [
      {
        label: "Member Open",
        strokeColor: "rgba(249, 189, 99, 1)",
        fillColor: "rgba(255, 255, 255, 0)",
        pointColor: "rgba(249, 189, 99, 1)",
        pointStrokeColor: "rgba(249, 189, 99, 1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(249, 189, 99, 1)",
        data: [65, 59, 80, 81, 56, 55, 5, 40, 55, 40, 40, 90, ]
      }
    ]
  };

  var PinjamanBulananOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 2,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  PinjamanBulanan.Line(PinjamanBulananData, PinjamanBulananOptions);

  //---------------------------------------
  //- END CHART CHART LINE TOTAL PINJAMAN -
  //---------------------------------------

  //---------------------------------------
  //- START PIE CHART Pengajuan Aktif dan Arsip -
  //---------------------------------------
  
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: 500,
      color: "rgba(0, 133, 72, 0.8)",
      highlight: "rgba(0, 133, 72, 1)",
      label: "Pengajuan Aktif"
    },
    {
      value: 100,
      color: "rgba(230, 204, 255, 0.8)",
      highlight: "rgba(230, 204, 255, 1)",
      label: "Pengajuan Arsip"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=value %> <%=label%> users"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);

  //-------------------------------------------
  //- END PIE CHART Pengajuan Aktif dan Arsip -
  //-------------------------------------------



  //----------------------------------------
  //- START PIE CHART Status Member Muaman -
  //----------------------------------------

  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChartDua").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: 500,
      color: "rgba(0, 133, 72, 0.8)",
      highlight: "rgba(0, 133, 72, 1)",
      label: "Member Open"
    },
    {
      value: 200,
      color: "rgba(46, 175, 244, 0.8)",
      highlight: "rgba(46, 175, 244, 1)",
      label: "Member Approve"
    },
    {
      value: 300,
      color: "rgba(249, 189, 99, 0.8)",
      highlight: "rgba(249, 189, 99, 1)",
      label: "Member Pending"
    },
    {
      value: 100,
      color: "rgba(230, 204, 255, 0.8)",
      highlight: "rgba(230, 204, 255, 1)",
      label: "Member Cancel"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=value %> <%=label%> users"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);

  //--------------------------------------
  //- END PIE CHART Status Member Muaman -
  //--------------------------------------

  /* jVector Maps
   * ------------
   * Create a world map with markers
   */
  $('#world-map-markers').vectorMap({
    map: 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity: 0.7,
    hoverColor: false,
    backgroundColor: 'transparent',
    regionStyle: {
      initial: {
        fill: 'rgba(210, 214, 222, 1)',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      },
      hover: {
        "fill-opacity": 0.7,
        cursor: 'pointer'
      },
      selected: {
        fill: 'yellow'
      },
      selectedHover: {
      }
    },
    markerStyle: {
      initial: {
        fill: '#00a65a',
        stroke: '#111'
      }
    },
    markers: [
      {latLng: [41.90, 12.45], name: 'Vatican City'},
      {latLng: [43.73, 7.41], name: 'Monaco'},
      {latLng: [-0.52, 166.93], name: 'Nauru'},
      {latLng: [-8.51, 179.21], name: 'Tuvalu'},
      {latLng: [43.93, 12.46], name: 'San Marino'},
      {latLng: [47.14, 9.52], name: 'Liechtenstein'},
      {latLng: [7.11, 171.06], name: 'Marshall Islands'},
      {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
      {latLng: [3.2, 73.22], name: 'Maldives'},
      {latLng: [35.88, 14.5], name: 'Malta'},
      {latLng: [12.05, -61.75], name: 'Grenada'},
      {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
      {latLng: [13.16, -59.55], name: 'Barbados'},
      {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
      {latLng: [-4.61, 55.45], name: 'Seychelles'},
      {latLng: [7.35, 134.46], name: 'Palau'},
      {latLng: [42.5, 1.51], name: 'Andorra'},
      {latLng: [14.01, -60.98], name: 'Saint Lucia'},
      {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
      {latLng: [1.3, 103.8], name: 'Singapore'},
      {latLng: [1.46, 173.03], name: 'Kiribati'},
      {latLng: [-21.13, -175.2], name: 'Tonga'},
      {latLng: [15.3, -61.38], name: 'Dominica'},
      {latLng: [-20.2, 57.5], name: 'Mauritius'},
      {latLng: [26.02, 50.55], name: 'Bahrain'},
      {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
    ]
  });

  /* SPARKLINE CHARTS
   * ----------------
   * Create a inline charts with spark line
   */

  //-----------------
  //- SPARKLINE BAR -
  //-----------------
  $('.sparkbar').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'bar',
      height: $this.data('height') ? $this.data('height') : '30',
      barColor: $this.data('color')
    });
  });

  //-----------------
  //- SPARKLINE PIE -
  //-----------------
  $('.sparkpie').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'pie',
      height: $this.data('height') ? $this.data('height') : '90',
      sliceColors: $this.data('color')
    });
  });

  //------------------
  //- SPARKLINE LINE -
  //------------------
  $('.sparkline').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'line',
      height: $this.data('height') ? $this.data('height') : '90',
      width: '100%',
      lineColor: $this.data('linecolor'),
      fillColor: $this.data('fillcolor'),
      spotColor: $this.data('spotcolor')
    });
  });
});
